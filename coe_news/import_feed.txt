$feeds_importer = new stdClass;
$feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
$feeds_importer->api_version = 1;
$feeds_importer->id = 'webtools_listbuilder_importer';
$feeds_importer->config = array(
  'name' => 'Webtools ListBuilder Importer to News and Spotlight',
  'description' => 'Bring in all webtools listbuilder data from all lists',
  'fetcher' => array(
    'plugin_key' => 'FeedsWtListBuilderFetcher',
    'config' => array(
      'listBuilderIds' => '32,991,976,4433,1147,959,141,1151,732,3878,323',
      'itemTypeToTaxonomyMapping' => 'Educational Psychology|college_units|Educational Psychology
Special Education|college_units|Special Education
Curriculum and Instruction|college_units|Curriculum & Instruction
EPOL|college_units|Education Policy, Organization and Leadership
',
    ),
  ),
  'parser' => array(
    'plugin_key' => 'FeedsWtListBuilderParser',
    'config' => array(),
  ),
  'processor' => array(
    'plugin_key' => 'FeedsNodeProcessor',
    'config' => array(
      'content_type' => 'article',
      'expire' => '-1',
      'author' => '1',
      'mappings' => array(
        0 => array(
          'source' => 'title',
          'target' => 'title',
          'unique' => FALSE,
        ),
        1 => array(
          'source' => 'id',
          'target' => 'guid',
          'unique' => 1,
        ),
        2 => array(
          'source' => 'terms',
          'target' => 'field_college_units',
          'unique' => FALSE,
        ),
        3 => array(
          'source' => 'pubDateTimeUnix',
          'target' => 'field_news_publishdate:start',
          'unique' => FALSE,
        ),
        4 => array(
          'source' => 'photo_url',
          'target' => 'field_webtools_image_url',
          'unique' => FALSE,
        ),
        5 => array(
          'source' => 'webtools_detail_url',
          'target' => 'field_webtools_url',
          'unique' => FALSE,
        ),
        6 => array(
          'source' => 'article_or_description',
          'target' => 'body',
          'unique' => FALSE,
        ),
        7 => array(
          'source' => 'link',
          'target' => 'field_external_url',
          'unique' => FALSE,
        ),
      ),
      'update_existing' => '0',
      'input_format' => 'plain_text',
    ),
  ),
  'content_type' => '',
  'update' => 0,
  'import_period' => '3600',
  'expire_period' => 3600,
  'import_on_create' => 1,
  'process_in_background' => 0,
);
