<?php

/**
 *
 * variables:
 *
 *  $news_items array of news items
 *  $year
 *  $context  = 'college_news' | 'people' | 'unit'
 *  $unit (unit site params for current unit context)
    $unit[unit_id] => epol
    $unit[unit_name] => Department of Education Policy, Organization & Leadership
    $unit[unit_short_name] => EPOL
    $unit[unit_url] => /epol
 */
//dpm($variables);
$classes = array('news-listing', 'news', $context);
if ($context == 'unit') {
  $classes[] = $unit['unit_id'];
}
$classes_string = join(" ", $classes);
?>


<div id="news-main-index" class=" <?php print $classes_string; ?>">
<?php $j = 0; foreach ($news_items as $i => $news_item) { $j++; //dpm('news item in theme'); dpm($news_item);
  $body = ($news_item['body_summary']) ? $news_item['body_summary'] : $news_item['body'];

  $detail_url = FALSE;
  if ($news_item['external_url']) {
    $detail_url = $news_item['external_url'];
  }
  elseif ($news_item['body'] && $news_item['path']) {
    $detail_url = $news_item['path'];
  }
  $more = ($detail_url) ? l('more...', $detail_url) : FALSE;
  $title = ($detail_url) ? l($news_item['title'], $detail_url) : $news_item['title'];
  $odd_even = (round($j/2,0) == $j/2) ? 'even' : 'odd';

?>
  <div class="news-item <?php print $odd_even; ?>">
    <div class="news-image">
      <img src="<?php print image_style_url('sidebar-news', $news_item['photo']['uri']); ?>" alt="<?php print $news_item['photo']['alt']; ?>" />
    </div>
    <div class="news-title-text">
      <h3 class="title"><span class="date"><?php print $news_item['news_publishdate_friendly']; ?></span> -- <?php print $title; ?></h3>
      <div class="news-text"><?php print $body . '<span class="more">' . $more . '</span>'; ?></div>
    </div>
  </div>
<?php } ?>
</div>
