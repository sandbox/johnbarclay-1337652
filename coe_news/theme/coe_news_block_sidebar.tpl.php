<?php

/**
 * variables:
 * $news_items array of individual items, each item has the following variables:
 *
      $title  - node title
      $body_summary - text of teaser.
      $body - generally not used and empty since news items have their own urls.

      $photo array of photo/image data needed for themed image
      $external_url (String, 52 characters ) http://news.illinois.edu/news/11/0223conference...
      news_publishdate  e.g. 2011-02-23 17:56:00
      news_publishdate_friendly e.g. June 26, 2011
      news_publishdate_numeric e.g. 6/26/2011

      **** not useful ***
      $type node type article
      $news_type will always be "news" for this news block
      $path.  drupal path to node.  not useful.
      $nid (String, 5 characters ) 75742
      $status (String, 9 characters ) Published
      $college_units (Integer) e.g. 3133
      $subject_area (Integer) e.g. 38
 */
//dpm($news_items);

?>
<div class="unit-side-news">
<?php foreach ($news_items as $i => $news_item) { //dpm($news_items); dpm($news_items);?>
  <div class="news-image">
    <a href="<?php print $news_item['external_url']; ?>" >
    <img src="<?php print image_style_url('sidebar-news', $news_item['photo']['uri']); ?>" alt="<?php print $news_item['photo']['alt']; ?>" />
    <p class="title"><?php print $news_item['title']; ?> <span class="date">(<?php print $news_item['news_publishdate_numeric']; ?>)</span></p>
    </a>
  </div>
<?php } ?>
</div>
