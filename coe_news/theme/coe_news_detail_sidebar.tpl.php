<?php

/**
 * variables:
 * $news_items array of individual items, each item has the following variables:
 *


 * $style = 'unit' or 'people'
 * $news_items = array of news items
 *
      $title  - node title
      $body_summary - text of teaser.
      $body - generally not used and empty since news items have their own urls.

      $photo array of photo/image data needed for themed image
      $external_url (String, 52 characters ) http://news.illinois.edu/news/11/0223conference...
      news_publishdate  e.g. 2011-02-23 17:56:00
      news_publishdate_friendly e.g. June 26, 2011
      news_publishdate_numeric e.g. 6/26/2011

      **** not useful ***
      $type node type article
      $news_type will always be "news" for this news block
      $path.  drupal path to node.  not useful.
      $nid (String, 5 characters ) 75742
      $status (String, 9 characters ) Published
      $college_units (Integer) e.g. 3133
      $subject_area (Integer) e.g. 38
 */
//dpm('variables'); dpm($variables); dpm($news_items);
extract($variables);
if (!isset($external_url)) {
  $external_url = NULL;
}
?>

<div class="side-bar-features features news">
<h2 class="cal-block news-block"><?php print l('news','news'); ?></h2>
<div class="news-items" id="news-items-homepage">
<?php $i=0; foreach ($news_items as $i => $news_item) {
  $i++;
  $odd_even = (round($i/2,0) == $i/2) ? 'even' : 'odd';
  extract($news_item); // turn array of variables into individual variables
  $more = l('more...', $external_url);
  ?>
  <div class="news-item news-item-<?php print $odd_even; ?>">
    <div class="news-image">
      <img class="thumbnail" src="<?php print image_style_url('sidebar-news', $photo['uri']); ?>" alt="<?php print $photo['alt']; ?>" />
    </div>
    <div class="news-title-text">
      <h3 class="title"><span class="title"><a href="<?php print $external_url; ?>" ><?php print $title; ?></a></span> <span class="date">(<?php print $news_publishdate_numeric; ?>)</span></h3>
      <div class="news-text"><?php print $body_summary . ' <span class="more">' . $more . '</span>'; ?></div>
    </div>
  </div>
<?php } ?>
</div>

</div>
