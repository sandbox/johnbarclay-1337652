<?php

$number_of_features = 5;
$iterator = 0;
?>
<div class="frontpage-top-features">
  <div class="spotlight-label">spotlight</div>
  <?php foreach ($rows as $id => $row) {
    $entity = $view->result[$id]->_field_data['nid']['entity'];
    $short_title = $entity->field_title_70_char['und'][0]['value'];
    $url = $entity->field_external_url ['und'][0]['value'];
    if ((count(@$entity->field_photo['und']) > 0) && ($iterator < $number_of_features)) { ?>
  <div class="<?php print $classes_array[$id]; ?> views-row-frontpage-feature views-row-frontpage-feature-' . ($iterator+1) ; ?>">
    <div class="feature-image">
      <img src="<?php print image_style_url('front-page-feature', $entity->field_photo['und'][0]['uri']) ; ?>" alt="Feature Photo" />
    </div>
    <div class="feature-story">
      <div class="views-field views-field-title">
        <span class="field-content">
          <a href="<?php print $url; ?>"><?php print $short_title; ?></a>
        </span>
      </div>
    </div>
  </div>
  <?php
        $iterator++;
       }
    }
  ?>
</div>

<script type="text/javascript">
(function ($) {
$("div.feature-story > div.views-field-title > span.field-content > a").hover(function() {
    $("div.feature-image").css('display','none');
		$(this).parents("div.views-row-frontpage-feature").find("div.feature-image").css('display','block');
 });
 })(jQuery);
 </script>
