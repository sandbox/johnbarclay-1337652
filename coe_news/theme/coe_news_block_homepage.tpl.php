<?php

/**
 * variables:
 * $news_items array of individual items, each item has the following variables:
 *


 * $style = 'unit' or 'people'
 * $news_items = array of news items
 *
      $title  - node title
      $body_summary - text of teaser.
      $body - generally not used and empty since news items have their own urls.

      $photo array of photo/image data needed for themed image
      $external_url (String, 52 characters ) http://news.illinois.edu/news/11/0223conference...
      news_publishdate  e.g. 2011-02-23 17:56:00
      news_publishdate_friendly e.g. June 26, 2011
      news_publishdate_numeric e.g. 6/26/2011

      **** not useful ***
      $type node type article
      $news_type will always be "news" for this news block
      $path.  drupal path to node.  not useful.
      $nid (String, 5 characters ) 75742
      $status (String, 9 characters ) Published
      $college_units (Integer) e.g. 3133
      $subject_area (Integer) e.g. 38
 */
//dpm($news_items);
?>
<div class="view view-news-views view-id-news_views view-display-id-front_page view-dom-id-1">
<div class="view-content">
<div class="frontpage-middle-features">
  <div class="news-label">
    news
  </div>
<?php $i=0; foreach ($news_items as $i => $news_item) { $i++; extract($news_item); //dpm($news_item); dpm($news_item['webtools_image_url']); ?>
 <div class="views-row views-row-7 views-row-odd views-row-frontpage-small-feature views-row-frontpage-small-feature-<?php print $i; ?>">
    <img src="<?php print image_style_url('front-page-small-feature', $photo['uri']); ?>" alt="<?php print $photo['alt']; ?>" />
    <div class="views-field views-field-title">
      <span class="field-content"><a href="<?php print $external_url; ?>" ><?php print $title; ?></a></span>
      <span class="date">(<?php print $news_publishdate_numeric; ?>)</span>
    </div>
    <div class="views-field views-field-entity-id">
      <div class="field-content">
        <?php print $body_summary; ?>
      </div>
    </div>
  </div>
<?php } ?>
</div>
</div>
</div>

