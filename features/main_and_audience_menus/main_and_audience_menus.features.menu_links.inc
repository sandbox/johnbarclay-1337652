<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function main_and_audience_menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:fac
  $menu_links['main-menu:fac'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'fac',
    'router_path' => 'fac',
    'link_title' => 'The Faculty',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: main-menu:http://education.illinois.edu/advancement/gifts/makeagift.html
  $menu_links['main-menu:http://education.illinois.edu/advancement/gifts/makeagift.html'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://education.illinois.edu/advancement/gifts/makeagift.html',
    'router_path' => '',
    'link_title' => 'Make a Gift',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-44',
  );
  // Exported menu link: main-menu:node/70185
  $menu_links['main-menu:node/70185'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/70185',
    'router_path' => 'node/%',
    'link_title' => 'About Us',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-45',
  );
  // Exported menu link: main-menu:node/70210
  $menu_links['main-menu:node/70210'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/70210',
    'router_path' => 'node/%',
    'link_title' => 'Departments & Units',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-46',
  );
  // Exported menu link: main-menu:node/70499
  $menu_links['main-menu:node/70499'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/70499',
    'router_path' => 'node/%',
    'link_title' => 'Curriculum & Instruction',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'node/70210',
  );
  // Exported menu link: main-menu:node/71048
  $menu_links['main-menu:node/71048'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/71048',
    'router_path' => 'node/%',
    'link_title' => 'Educational Psychology',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'node/70210',
  );
  // Exported menu link: main-menu:node/71157
  $menu_links['main-menu:node/71157'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/71157',
    'router_path' => 'node/%',
    'link_title' => 'Education Policy, Organization & Leadership',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'node/70210',
  );
  // Exported menu link: main-menu:node/71516
  $menu_links['main-menu:node/71516'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/71516',
    'router_path' => 'node/%',
    'link_title' => 'Impact in the World',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: main-menu:node/72157
  $menu_links['main-menu:node/72157'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72157',
    'router_path' => 'node/%',
    'link_title' => 'Academic Programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-47',
  );
  // Exported menu link: main-menu:node/72166
  $menu_links['main-menu:node/72166'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72166',
    'router_path' => 'node/%',
    'link_title' => 'Graduate Programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/72157',
  );
  // Exported menu link: main-menu:node/72168
  $menu_links['main-menu:node/72168'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72168',
    'router_path' => 'node/%',
    'link_title' => 'Online Programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '2',
    'parent_path' => 'node/72157',
  );
  // Exported menu link: main-menu:node/72183
  $menu_links['main-menu:node/72183'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72183',
    'router_path' => 'node/%',
    'link_title' => 'Undergraduate Programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '1',
    'parent_path' => 'node/72157',
  );
  // Exported menu link: main-menu:node/72311
  $menu_links['main-menu:node/72311'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72311',
    'router_path' => 'node/%',
    'link_title' => 'Research & Outreach',
    'options' => array(
      'attributes' => array(),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '1',
    'weight' => '-48',
  );
  // Exported menu link: main-menu:node/72667
  $menu_links['main-menu:node/72667'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72667',
    'router_path' => 'node/%',
    'link_title' => 'Special Education',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
    'parent_path' => 'node/70210',
  );
  // Exported menu link: main-menu:node/72858
  $menu_links['main-menu:node/72858'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72858',
    'router_path' => 'node/%',
    'link_title' => 'Strategic Initiatives',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
    'parent_path' => 'node/70210',
  );
  // Exported menu link: main-menu:node/72859
  $menu_links['main-menu:node/72859'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72859',
    'router_path' => 'node/%',
    'link_title' => 'Research & Outreach Units',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-45',
    'parent_path' => 'node/70210',
  );
  // Exported menu link: main-menu:node/72860
  $menu_links['main-menu:node/72860'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/72860',
    'router_path' => 'node/%',
    'link_title' => 'Administrative Units',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-44',
    'parent_path' => 'node/70210',
  );
  // Exported menu link: menu-audience-menu:<front>
  $menu_links['menu-audience-menu:<front>'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Faculty & Staff',
    'options' => array(
      'attributes' => array(
        'title' => 'Information for college faculty & staff',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: menu-audience-menu:http://education.illinois.edu/advancement/gifts/makeagift.html
  $menu_links['menu-audience-menu:http://education.illinois.edu/advancement/gifts/makeagift.html'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'http://education.illinois.edu/advancement/gifts/makeagift.html',
    'router_path' => '',
    'link_title' => 'Make a Gift',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => '<front>',
  );
  // Exported menu link: menu-audience-menu:http://education.illinois.edu/ecso
  $menu_links['menu-audience-menu:http://education.illinois.edu/ecso'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'http://education.illinois.edu/ecso',
    'router_path' => '',
    'link_title' => 'Career Services (ECSO)',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-40',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/70264
  $menu_links['menu-audience-menu:node/70264'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/70264',
    'router_path' => 'node/%',
    'link_title' => 'Research Resources',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => '<front>',
  );
  // Exported menu link: menu-audience-menu:node/70624
  $menu_links['menu-audience-menu:node/70624'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/70624',
    'router_path' => 'node/%',
    'link_title' => 'Technology Resources',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => '<front>',
  );
  // Exported menu link: menu-audience-menu:node/70993
  $menu_links['menu-audience-menu:node/70993'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/70993',
    'router_path' => 'node/%',
    'link_title' => 'Students',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: menu-audience-menu:node/70994
  $menu_links['menu-audience-menu:node/70994'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/70994',
    'router_path' => 'node/%',
    'link_title' => 'Advising & Support',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-42',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/71520
  $menu_links['menu-audience-menu:node/71520'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/71520',
    'router_path' => 'node/%',
    'link_title' => 'International Programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/72157
  $menu_links['menu-audience-menu:node/72157'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/72157',
    'router_path' => 'node/%',
    'link_title' => 'Admissions',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-45',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/72166
  $menu_links['menu-audience-menu:node/72166'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/72166',
    'router_path' => 'node/%',
    'link_title' => 'Graduate Programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/72183
  $menu_links['menu-audience-menu:node/72183'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/72183',
    'router_path' => 'node/%',
    'link_title' => 'Undergraduate Programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/72368
  $menu_links['menu-audience-menu:node/72368'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/72368',
    'router_path' => 'node/%',
    'link_title' => 'Financial Aid',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-44',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/72370
  $menu_links['menu-audience-menu:node/72370'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/72370',
    'router_path' => 'node/%',
    'link_title' => 'Forms',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-41',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/72376
  $menu_links['menu-audience-menu:node/72376'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/72376',
    'router_path' => 'node/%',
    'link_title' => 'Honors Programs',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/72863
  $menu_links['menu-audience-menu:node/72863'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/72863',
    'router_path' => 'node/%',
    'link_title' => 'Student Groups',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'node/70993',
  );
  // Exported menu link: menu-audience-menu:node/72865
  $menu_links['menu-audience-menu:node/72865'] = array(
    'menu_name' => 'menu-audience-menu',
    'link_path' => 'node/72865',
    'router_path' => 'node/%',
    'link_title' => 'Update Your Information',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => '<front>',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About Us');
  t('Academic Programs');
  t('Administrative Units');
  t('Admissions');
  t('Advising & Support');
  t('Career Services (ECSO)');
  t('Curriculum & Instruction');
  t('Departments & Units');
  t('Education Policy, Organization & Leadership');
  t('Educational Psychology');
  t('Faculty & Staff');
  t('Financial Aid');
  t('Forms');
  t('Graduate Programs');
  t('Honors Programs');
  t('Impact in the World');
  t('International Programs');
  t('Make a Gift');
  t('Online Programs');
  t('Research & Outreach');
  t('Research & Outreach Units');
  t('Research Resources');
  t('Special Education');
  t('Strategic Initiatives');
  t('Student Groups');
  t('Students');
  t('Technology Resources');
  t('The Faculty');
  t('Undergraduate Programs');
  t('Update Your Information');


  return $menu_links;
}
