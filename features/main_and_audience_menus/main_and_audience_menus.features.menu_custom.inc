<?php

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function main_and_audience_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-audience-menu
  $menus['menu-audience-menu'] = array(
    'menu_name' => 'menu-audience-menu',
    'title' => 'Audience Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Audience Menu');
  t('Main menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');


  return $menus;
}
