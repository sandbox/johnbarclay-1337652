<?php

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function test_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-test-menu-for-theming
  $menus['menu-test-menu-for-theming'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'title' => 'Test Menu for theming',
    'description' => 'Test Menu for theming',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Test Menu for theming');


  return $menus;
}
