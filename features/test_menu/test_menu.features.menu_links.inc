<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function test_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-test-menu-for-theming:http://education.illinois.edu
  $menu_links['menu-test-menu-for-theming:http://education.illinois.edu'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'link_path' => 'http://education.illinois.edu',
    'router_path' => '',
    'link_title' => 'College of Education Current Site',
    'options' => array(
      'attributes' => array(
        'title' => 'sample expanded links',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '1',
    'expanded' => '1',
    'weight' => '-49',
  );
  // Exported menu link: menu-test-menu-for-theming:http://education.illinois.edu/ci
  $menu_links['menu-test-menu-for-theming:http://education.illinois.edu/ci'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'link_path' => 'http://education.illinois.edu/ci',
    'router_path' => '',
    'link_title' => 'CI',
    'options' => array(
      'attributes' => array(
        'title' => 'middle link of expanded menu tree',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'http://education.illinois.edu',
  );
  // Exported menu link: menu-test-menu-for-theming:http://education.illinois.edu/edpsy
  $menu_links['menu-test-menu-for-theming:http://education.illinois.edu/edpsy'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'link_path' => 'http://education.illinois.edu/edpsy',
    'router_path' => '',
    'link_title' => 'Edpsy',
    'options' => array(
      'attributes' => array(
        'title' => 'last item in expanded menu tree',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'http://education.illinois.edu',
  );
  // Exported menu link: menu-test-menu-for-theming:http://education.illinois.edu/epol
  $menu_links['menu-test-menu-for-theming:http://education.illinois.edu/epol'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'link_path' => 'http://education.illinois.edu/epol',
    'router_path' => '',
    'link_title' => 'EPOL',
    'options' => array(
      'attributes' => array(
        'title' => 'first child of expanded item',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'http://education.illinois.edu',
  );
  // Exported menu link: menu-test-menu-for-theming:node/28640
  $menu_links['menu-test-menu-for-theming:node/28640'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'link_path' => 'node/28640',
    'router_path' => 'node/%',
    'link_title' => 'EPOL',
    'options' => array(
      'attributes' => array(
        'title' => 'top menu item if collapsed tree',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'node/46',
  );
  // Exported menu link: menu-test-menu-for-theming:node/28641
  $menu_links['menu-test-menu-for-theming:node/28641'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'link_path' => 'node/28641',
    'router_path' => 'node/%',
    'link_title' => 'Edpsy',
    'options' => array(
      'attributes' => array(
        'title' => 'bottom link in default collapsed men',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'node/46',
  );
  // Exported menu link: menu-test-menu-for-theming:node/43496
  $menu_links['menu-test-menu-for-theming:node/43496'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'link_path' => 'node/43496',
    'router_path' => 'node/%',
    'link_title' => 'CI',
    'options' => array(
      'attributes' => array(
        'title' => 'middle menu item in default collapsed menu tree',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'node/46',
  );
  // Exported menu link: menu-test-menu-for-theming:node/46
  $menu_links['menu-test-menu-for-theming:node/46'] = array(
    'menu_name' => 'menu-test-menu-for-theming',
    'link_path' => 'node/46',
    'router_path' => 'node/%',
    'link_title' => 'College of Education Future Site',
    'options' => array(
      'attributes' => array(
        'title' => 'top menu item in default collapsed menu tree',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '4',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('CI');
  t('College of Education Current Site');
  t('College of Education Future Site');
  t('EPOL');
  t('Edpsy');


  return $menu_links;
}
