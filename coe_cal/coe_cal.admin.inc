<?php


/**
 * admin form for webtools settings
 **/

function coe_cal_settings() {

  $form['#title'] = "Configure COE Calendar Settings";

  $form['general']['coe_cal_pages'] = array(
    '#type' => 'textarea',
    '#title' => t("Calendar Pages"),
    '#default_value' => variable_get('coe_cal_paths',NULL),
    '#cols' => 70,
    '#rows' => 6,
    '#weight' => 11,
    '#description' => t('Custom Pages for calendars.  Enter in form:
       <pre>path|comma separated webtools calendar ids|page title</pre>

     with each page separated by a line return.
     <br/><pre>
       advancement/calendar|342,5423|Advancement Calendar
       college_fun/calendar|3243,324234|Entertainment Calendars
       </pre>'),
  );

  return system_settings_form($form);
}
