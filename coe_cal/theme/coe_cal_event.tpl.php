<?php


//dpm($variables);

$sections = array(
  'Speaker' => 'wt_cal_speaker',
  'Date' => 'date_display',
  'Time' => 'time_display',
  'Location' => 'wt_cal_location',
  'Cost' => 'wt_cal_cost',
  'Sponsor' => 'wt_cal_sponsor',
  'Contact' => 'wt_cal_contact_name',
  'E-Mail' => 'wt_cal_contact_email',
  'Phone' => 'wt_cal_contact_phone',
);
drupal_set_title($event['title']);
?>
<!-- begin template modules/all/coe/coe_cal/theme/coe_cal_event.tpl.php -->
<div id="event-wrapper">
<?php
foreach ($sections as $heading => $field_name) {
 if (isset($event[$field_name]) && $event[$field_name]) { ?>
<div class="detail-row">
<span class="column-label"><?php print $heading; ?></span>
<span class="column-info"><?php print $event[$field_name]; ?></span>
<div class="ws-cal-clear"></div>
</div>

<?php  }
}
?>
</div>
<!-- end template modules/all/coe/coe_cal/theme/coe_cal_event.tpl.php -->
