<?php

/**
 * variables:
 * $style = 'home' or 'unit'
 * $calendar_items event calendar item array (see coe_cal_calendar_items_variables.txt file)
 *
 */

//dpm($variables);
$full_calendar_path = ($style = 'unit') ? 'calendar' : 'calendar';
?>
<h2><?php print l('EVENTS', $full_calendar_path); ?></h2>
<div class="unit-side-cal <?php print "unit-side-cal-" . $style; ?>">
<ol class="cal-index">
<?php foreach ($calendar_items as $date => $calendar_items) { ?>
<?php foreach ($calendar_items['items'] as $i => $calendar_item) { extract($calendar_item); ?>
  <li class="calender-list-item">
    <?php print l($title, $detail_path); ?>
    <span class="date"><?php print date('M j',  $event_date_time_unixtime) . ' ' . $time_display; ?></span>
  </li>
<?php } ?>
<?php } ?>
<ol>
</div>
