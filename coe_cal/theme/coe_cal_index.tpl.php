<?php

/**
 *
 * variables:
 *
 * $calendar_items event calendar item array (see coe_cal_calendar_items_variables.txt file)
 *
 *  $year
 *  $month
 *  $event_id
 *  $context
 *  $unit
 *  $theme_hook_suggestions
 *  $zebra
 *  $id
 *  $directory
 *  $classes_array
 *  $attributes_array
 *  $title_attributes_array
 *  $content_attributes_array
 *  $title_prefix
 *  $title_suffix
 *  $user
 *  $db_is_active
 *  $is_admin
 *  $logged_in
 *  $is_front
 *  $classes
 *  $attributes
 *  $title_attributes
 *  $content_attributes


 *
 */
//dpm($variables);
//print "<pre>"; print join("'\n *  '", array_keys($calendar_items['2011-12-28 23:30:00']['items'][0]));
$classes = array('cal-listing', 'cal', $context);
if ($context == 'unit') {
  $classes[] = $unit['unit_id'];
}
$classes_string = join(" ", $classes);
?>
<!-- begin template modules/all/coe/coe_cal/theme/coe_cal_index.tpl.php -->
<div id="cal-index" class="<?php print $classes_string; ?>">
<ol>
<?php foreach ($calendar_items as $day => $day_data) {
   //dpm('news item in theme'); dpm($calendar_item); ?>
  <li>
    <h2 class="dayHeader"><?php print $day_data['date_friendly']; ?></h2>
    <ol class="calender-item">
    <?php
      $j = 0;
      foreach ($day_data['items'] as $i => $item) {
        $j++;
        $odd_even = (round($j/2,0) == $j/2) ? 'even' : 'odd';
        //dpm($item);
        extract($item);
        $edit_link  =  '';//($logged_in) ?  ' ' . l('edit', 'node/'. $nid . '/edit') : '';
      ?>
      <li class="<?php print $odd_even; ?>">
        <span class="event-time"><?php print $time_display ?></span>
        <span class="event-list-info">
          <span class="event-name">
            <a href="<?php print $detail_path; ?>"><?php print $wt_cal_title_long; ?></a><?php print $edit_link; ?>
          </span>
        </span>
        <div class="ws-cal-clear"></div>
        <div class="ws-cal-clear"></div>
      </li>
    <?php } ?>
    </ol>
  </li>
 <?php } ?>
 </ol>
 </div>
<!-- end template modules/all/coe/coe_cal/theme/coe_cal_index.tpl.php -->
